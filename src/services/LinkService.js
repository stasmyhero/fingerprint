import { LinkDTO } from '../dto/LinkDTO.js';
import { ApiError } from '../errors/ApiError.js';
import { codeURL, encodeBase64 } from '../utils/common.js';
import linkModel from '../models/linkModel.js';
import { TokenService } from './TokenService.js';

export class LinkService {
  static async generate({ isoDate, timestamp, recipient }, refreshToken) {
    const url = codeURL(isoDate, recipient);
    const creator = await TokenService.verify(refreshToken);
    if (!creator) {
      throw ApiError.Unauthorized();
    }
    const result = await linkModel.create({
      url,
      isoDate,
      created_at: timestamp,
      recipient,
      creator: 'admin'
    });
    if (!result) {
      throw ApiError.BadRequest('Ошибка при записи ссылки в БД');
    }
    const link = new LinkDTO(result);
    return link;
  }

  static async getAll() {
    const links = await linkModel.find().exec();
    if (links) {
      return links.length ? links.map((link) => new LinkDTO(link)) : [];
    } else throw ApiError.BadRequest('Нет записей');
  }

  static async getByID(id) {
    const link = await linkModel.finByID(id);
    if (link) {
      return new LinkDTO(link);
    } else throw ApiError.BadRequest('Ссылка с таким id не найдена');
  }

  static async deleteByID(id) {
    const link = await recordsModel.finByID(id);
    if (link) {
      return linkModel.deleteByID(id);
    } else throw ApiError.BadRequest('Ссылка с таким id не найдена');
  }
}
