import { RecordDTO } from '../dto/RecordDTO.js';
import { ApiError } from '../errors/ApiError.js';
import recordsModel, { createRecord } from '../models/recordsModel.js';
import fetch from 'node-fetch';

export class RecordService {
  /**
   *
   * @param {*} filter
   * @param {*} limit
   * @param {*} offset
   * @returns
   */
  static async getAll(filter, limit, offset) {
    const records = await recordsModel.find().exec();
    // .skip(offset ?? 0)
    // .limit(limit ?? 100);
    if (records) {
      return records.length ? records.map((record) => new RecordDTO(record)) : [];
    } else throw ApiError.BadRequest('Нет записей');
  }

  /**
   *
   * @param {*} id
   * @returns
   */
  static async get(id) {
    const record = await recordsModel.findById(id);
    if (record) {
      return new RecordDTO(record);
    } else {
      throw ApiError.BadRequest('Запись  с таким id не найдена');
    }
  }

  /**
   *
   * @param {*} id
   * @returns
   */
  static async delete(id) {
    const record = await recordsModel.findById(id);
    if (record) {
      return recordsModel.findByIdAndDelete(record._id);
    } else {
      throw ApiError.BadRequest('Запись  с таким id не найдена');
    }
  }
  /**
   *
   * @param {*} record
   * @returns
   */
  static async insert(record) {
    const result = await recordsModel.collection.insertOne(record);
    return result;
  }
  /**
   *
   * @param {*} id
   * @param {*} record
   * @returns
   */
  static async update(id, record) {
    const result = recordsModel.findById(id);
    if (result) {
      return recordsModel.findByIdAndUpdate({ _id: id }, record);
    }
  }
  /**
   *
   * @returns
   */
  static async deleteAll() {
    const result = recordsModel.deleteMany({});
    return result;
  }

  static async take({ d, r, isoDate, date, id, requestID, link }) {
    if (id && requestID) {
      const response = await fetch(
        `${process.env.APP_FP_SERVICE_URL}/visitors/${id}?token=${process.env.APP_FP_SERVICE_TOKEN}`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/json'
            // 'Auth-Token': process.env.APP_FP_SERVICE_TOKEN
          }
        }
      );
      if (response.status === 200) {
        const data = await response.json();
        const record = createRecord(data.visits[0], data.visits.length, isoDate, r || '', id, requestID, link);
        const result = await recordsModel.create(record);
        return result;
      }
    } else {
      throw ApiError.BadGateway();
    }
    return {};
  }
}
