import { UserDTO } from '../dto/UserDTO.js';
import { ApiError } from '../errors/ApiError.js';
import userModel from '../models/userModel.js';
import { TokenService } from './TokenService.js';

export class UserService {
  /**
   * Login with email and password, throw errors if no email or passpowrd in DB
   * @param {string} email
   * @param {string} password
   * @returns
   */
  static async login({ email, password }) {
    const user = await userModel.findOne({ email });
    if (!user) {
      throw ApiError.BadRequest('Пользователь с таким адресом не найден');
    }
    if (password !== user.password) {
      throw ApiError.Unauthorized('Неверный пароль');
    }
    return this.getUserAndTokens(user);
  }
  /**
   * Logout (delete token from DB)
   * @param {string} refreshToken
   * @returns {string}
   */
  static async logout(refreshToken) {
    const token = await TokenService.delete(refreshToken);
    return token;
  }

  /**
   * Refresh token with verification
   * @param {*} refreshToken
   * @returns
   */
  static async refresh(refreshToken) {
    const result = await TokenService.verify(refreshToken);
    if (!result) {
      throw ApiError.Unauthorized();
    }
    const user = userModel.findById(result.id);
    return this.getUserAndTokens(user);
  }

  /**
   * Get user and tokens
   * @param {*} user
   * @returns
   */
  static async getUserAndTokens(user) {
    const userDTO = new UserDTO(user);
    const tokens = TokenService.generate(userDTO);
    await TokenService.save(userDTO.id, tokens.refreshToken);
    return { ...tokens, ...userDTO };
  }
}
