import jwt from 'jsonwebtoken';
import jwtModel from '../models/jwtModel.js';

export class TokenService {
  /**
   * Generate and return refresh and access tokens
   * @param {*} payload
   * @returns
   */

  static generate(payload) {
    const accessToken = jwt.sign({ ...payload }, process.env.JWT_SECRET, { expiresIn: '30m' });
    const refreshToken = jwt.sign({ ...payload }, process.env.JWT_REFRESH_SECRET, { expiresIn: '30d' });
    return {
      accessToken,
      refreshToken
    };
  }

  /**
   * Save token in DB by userID,
   * create tokens in DB if there is no tokens for this user
   * @param {*} userID
   * @param {*} refreshToken
   * @returns
   */
  static async save(userID, refreshToken) {
    const jwt = await jwtModel.findOne({ user: userID });
    if (jwt) {
      jwt.refreshToken = refreshToken;
      return jwt.save();
    }
    const token = await jwtModel.create({ userID: userID, refreshToken });
    return token;
  }

  /**
   * Delete jwt token from DB
   * @param {*} refreshToken
   * @returns
   */
  static async delete(refreshToken) {
    const token = jwtModel.deleteOne({ refreshToken });
    return token;
  }

  /**
   * verify refresh token with jwt verify and get it from db
   * @param {*} refreshToken
   * @returns false on fail, userData if success
   */
  static async verify(refreshToken) {
    try {
      const userData = jwt.verify(refreshToken, process.env.JWT_REFRESH_SECRET);
      // const tokenDB = jwtModel.findOne({ refreshToken });
      const tokenDB = true;
      return userData && tokenDB ? userData : false;
    } catch (e) {
      return false;
    }
  }

  /**
   * verify access token with jwt verify and get it from db
   * @param {*} accessToken
   * @returns false on fail, userData if success
   */
  static async verifyAccess(accessToken) {
    try {
      const userData = jwt.verify(accessToken, process.env.JWT_SECRET);
      // const tokenDB = jwtModel.findOne({ accessToken });
      return userData ? userData : false;
    } catch (e) {
      return false;
    }
  }
}
