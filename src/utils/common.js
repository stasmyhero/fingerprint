export const encodeBase64 = (str = '') => Buffer.from(str).toString('base64');

export const decodeBase64 = (str = '') => Buffer.from(str, 'base64').toString('ascii');

export const codeURL = (isoDate, recipient) => {
  console.log(encodeURI(isoDate));
  return `${process.env.APP_BASE_GEN_URL}/?d=${encodeURI(encodeBase64(isoDate))}&r=${encodeURI(
    encodeBase64(recipient)
  )}`;
};
