import Validator from 'fastest-validator';

const validator = new Validator();

/**
 * Validates id to be numeric
 * @param {*} id
 * @returns
 */
export const validateID = (id) =>
  validator.validate(
    { id },
    {
      id: {
        type: 'number',
        positive: true
      }
    }
  );

/**
 * Validates email addres
 * @param {*} email
 * @returns
 */

export const validateEmail = (email) =>
  validator.validate(
    { email },
    {
      email: {
        type: 'email'
      }
    }
  );

/**
 * Validates payload data which is needed to generate link
 * @param {*} payload
 * @returns
 */
export const validateGenerateLinkPayload = (payload) =>
  validator.validate(JSON.parse(JSON.stringify(payload)), {
    recipient: {
      type: 'multi',
      rules: [
        {
          type: 'email'
        },
        { type: 'string', empty: true }
      ]
    },
    isoDate: {
      type: 'date',
      convert: true
    },
    timestamp: {
      type: 'date'
    }
  });
