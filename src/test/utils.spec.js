import { codeURL } from '../utils/common';
import { config } from 'dotenv';
config();

describe('check encode and code url', () => {
  it('should return empty recipient when recipient is undefined', () => {});
  const recipient = undefined;
  const isoDate = '2022-03-11T18:00:00.000Z';
  expect(codeURL(isoDate, recipient)).toEqual('https://obzor.io/?d=MjAyMi0wMy0xMVQxODowMDowMC4wMDBaCg==');
});
