import { ApiError } from '../errors/ApiError.js';
import { LinkService } from '../services/LinkService.js';
import { validateID } from '../utils/validation.js';

export class LinkController {
  /**
   * Gets all link as DTO and returns in response
   * @returns
   */
  async getAll(_req, res, next) {
    try {
      const links = await LinkService.getAll();
      return res.json(links);
    } catch (err) {
      next(err);
    }
  }
  /**
   * Gets link by ID as DTO and returns it in response
   * @returns
   */
  async getByID(req, res, next) {
    try {
      const id = req.params?.id;
      const result = validateID(id);
      if (result !== true) {
        throw ApiError.BadRequest();
      }
      const links = await LinkService.getByID(id);
      return res.json(link);
    } catch (err) {
      next(err);
    }
  }
  /**
   * Generate link by incoming data
   * writes generated link inot database
   * @returns
   */
  async generate(req, res, next) {
    try {
      const { isoDate, timestamp, recipient } = req.body ?? {};
      if (!isoDate || !timestamp) {
        throw ApiError.BadRequest('Не указана дата создания');
      }
      const link = await LinkService.generate({ isoDate, timestamp, recipient }, req.cookies.refreshToken);
      return res.json(link);
    } catch (err) {
      next(err);
    }
  }

  /**
   * Delete link from db by ID
   */
  async delete(req, res, next) {
    try {
      const id = req.params?.id;
      const validationResult = validateID(id);
      if (validationResult === true) {
        return res(LinkService.deleteByID(id));
      } else {
        throw ApiError.BadGateway(validationResult?.[0]?.message);
      }
    } catch (err) {
      next(err);
    }
  }
}
