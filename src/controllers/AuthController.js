import { ApiError } from '../errors/ApiError.js';
import { UserService } from '../services/UserService.js';
import { validateEmail } from '../utils/validation.js';

export class AuthController {
  /**
   *
   * @returns
   */
  async login(req, res, next) {
    try {
      const { email, password } = req?.body;
      const validationResult = validateEmail(email);
      if (validationResult !== true) {
        throw ApiError.BadRequest('Неверный формат email-адреса');
      }
      const userData = await UserService.login({ email, password });
      res.cookie('refreshToken', userData.refreshToken, { maxAxge: 30 * 24 * 60 * 60 * 100, httpOnly: true });
      return res.json(userData);
    } catch (err) {
      next(err);
    }
  }
  /**
   *
   */
  async logout(req, res, next) {
    try {
      const { refreshToken } = req.cookies ?? {};
      const token = await UserService.logout(refreshToken);
      if (!token) {
        return next(ApiError.BadRequest('Ошибка обновления статуса авторизации', errors));
      }
      res.clearCookie('refreshToken');
      return res.json(token);
    } catch (err) {
      next(err);
    }
  }
  /**
   *
   */
  async refresh(req, res, next) {
    try {
      const { refreshToken } = req.cookies ?? {};
      if (!refreshToken) {
        next(ApiError.Unauthorized());
      }
      const userData = await UserService.refresh(refreshToken);
      res.cookie('refreshToken', userData.refreshToken, { maxAxge: 30 * 24 * 60 * 60 * 100, httpOnly: true });
      return res.json(userData);
    } catch (err) {
      next(err);
    }
  }
}
