import { ApiError } from '../errors/ApiError.js';
import { RecordService } from '../services/RecordService.js';

export class RecordController {
  async getAll(req, res, next) {
    try {
      const { limit, offset, filter } = req.params ?? {};
      const records = await RecordService.getAll(filter ?? '', limit, offset);
      return res.json(records);
    } catch (err) {
      next(err);
    }
  }

  async getByID(req, res, next) {
    try {
      const { id } = req.params ?? {};
      const record = await RecordService.get(id);
      if (!record) {
        throw ApiError.BadRequest('Такой записи не существует');
      }
      return res.json(record);
    } catch (err) {
      next(err);
    }
  }

  async put(req, res, next) {
    try {
      const { record } = req.data;
      if (!record) {
        throw ApiError.BadRequest('Не указаны данные для обновления');
      }
      const result = await RecordService.put(record);
      return res.json(result);
    } catch (e) {
      next(err);
    }
  }

  async post(req, res, next) {
    try {
      const { record } = req.data;
      if (!record) {
        throw ApiError.BadRequest('Не указаны данные для добавления');
      }
      const result = await RecordService.insert(record);
      return res.json(result);
    } catch (e) {
      next(err);
    }
  }

  async delete(req, res, next) {
    try {
      const { id } = req.params;
      if (!id) {
        throw ApiError.BadRequest();
      }
      const result = await RecordService.delete(id);
      return res.json(result);
    } catch (e) {
      next(err);
    }
  }
  async take(req, res, next) {
    try {
      if (!req.body) {
        throw ApiError.BadRequest();
      } else {
        const result = RecordService.take(req.body);
        return res.json(result);
      }
    } catch (err) {
      next(err);
    }
  }
}
