export class ApiError extends Error {
  status = 502;
  errors = [];

  constructor(status, message, errors) {
    super(message);
    this.status = status;
    this.errors = errors;
  }

  static Unauthorized(message = 'Пользователь не авторизован') {
    return new ApiError(401, message, []);
  }

  static BadRequest(message = 'Неверный запрос', errors = []) {
    return new ApiError(400, message, errors);
  }

  static BadGateway(message = 'Ошибка сервера', errors = []) {
    return new ApiError(502, message, errors);
  }

  static NotFound(message = 'Страница не найдена', errors = []) {
    return new ApiError(404, message, errors);
  }
}
