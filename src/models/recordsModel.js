import mongoose from 'mongoose';

const { Schema, model } = mongoose;

const RecordSchema = new Schema({
  // When fingerptint is getted
  isoDate: { type: Schema.Types.String, required: true },
  timestamp: { type: Schema.Types.Number },
  recipient: { type: String },
  requestID: { type: Schema.Types.String, required: true },
  // fingerprint info
  fingerprint: { type: Schema.Types.String, required: true },
  visitor: { type: Schema.Types.String },
  device: { type: Schema.Types.String },
  ip: { type: Schema.Types.String, required: true },
  os: { type: Schema.Types.String, required: true },
  version: { type: Schema.Types.String },
  visits: { type: Schema.Types.Number },
  browser: { type: Schema.Types.Object },
  // locarion info
  city: { type: Schema.Types.Object },
  country: { type: Schema.Types.Object },
  subdivision: { type: Schema.Types.Object },
  time: { type: Schema.Types.String }
});

export const createRecord = (fpData, visits, isoDate, recipient, visitorID, requestID) => ({
  isoDate,
  requestID,
  visits,
  recipient,
  fingerprint: visitorID,
  device: fpData.device,
  ip: fpData.ip,
  os: fpData.browserDetails.os,
  osVersion: fpData.browserDetails.osVersion,
  browser: fpData.browserDetails,
  city: fpData.ipLocation.city,
  country: fpData.ipLocation.country,
  subdivision: fpData.ipLocation.subdivision,
  time: fpData.time,
  timestamp: fpData.timestamp
});

export default model('Record', RecordSchema);
