import mongoose from 'mongoose';

const { Schema, model } = mongoose;

const UserSchema = new Schema({
  email: { type: Schema.Types.String, unique: true, required: true },
  password: { type: Schema.Types.String, unique: true, required: true }
});

export default model('User', UserSchema);
