import mongoose from 'mongoose';

const { Schema, model } = mongoose;

const LinkSchema = new Schema({
  url: { type: Schema.Types.String, required: true },
  created_at: { type: Schema.Types.Date, required: true },
  isoDate: { type: Schema.Types.String, required: true },
  recipient: { type: Schema.Types.String },
  creator: { type: Schema.Types.String, required: true }
});

export default model('Link', LinkSchema);
