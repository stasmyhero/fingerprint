import { Router } from 'express';
import { AuthController } from '../controllers/AuthController.js';

export const authRouter = new Router();
const controller = new AuthController();
authRouter.post('/login', controller.login);
authRouter.post('/logout', controller.logout);
authRouter.get('/refresh', controller.refresh);
