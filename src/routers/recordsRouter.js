import { Router } from 'express';
import { RecordController } from '../controllers/RecordController.js';
import authMiddleware from '../middleware/authMiddleware.js';

export const recordRouter = new Router();
const controller = new RecordController();

recordRouter.get('/all', authMiddleware, controller.getAll);
recordRouter.get('/', authMiddleware, controller.getByID);
recordRouter.put('/', authMiddleware, controller.put);
recordRouter.delete('/', authMiddleware, controller.delete);
recordRouter.post('/t', (res, req, next) => next(), controller.take);
