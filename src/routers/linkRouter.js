import { Router } from 'express';
import { LinkController } from '../controllers/LinkController.js';
import authMiddleware from '../middleware/authMiddleware.js';

export const linkRouter = new Router();
const controller = new LinkController();

linkRouter.use(authMiddleware);

linkRouter.get('/all', controller.getAll);
linkRouter.get('/', controller.getByID);
linkRouter.delete('/', controller.delete);
linkRouter.post('/generate', controller.generate);
