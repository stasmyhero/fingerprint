import { ApiError } from '../errors/ApiError.js';

export const errorHandler = (err, res, req, next) => {
  if (err instanceof ApiError) {
    return res.res.status(err.status).json({ message: err.message, errors: err.errors });
  }
  console.log(err);
  return errorHandler(ApiError.BadGateway(err.message ?? 'Ошибка сервера'), res, req, next);
};
``;
