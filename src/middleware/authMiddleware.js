import { ApiError } from '../errors/ApiError.js';
import { TokenService } from '../services/TokenService.js';

export default function (req, res, next) {
  try {
    if (!req.headers.authorization) {
      throw ApiError.Unauthorized();
    }
    const accessToken = req.headers.authorization.split(' ')[1];
    const result = TokenService.verifyAccess(accessToken);
    if (!result) {
      throw ApiError.Unauthorized();
    } else {
      req.user = result;
      next();
    }
  } catch (err) {
    return next(err);
  }
}
