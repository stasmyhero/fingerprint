import { BaseDTO } from './BaseDTO.js';
import countries from 'localized-countries';

const ruCountires = countries('ru_RU');

export class RecordDTO extends BaseDTO {
  constructor(record) {
    super(record);
    this.ip = record.ip;
    this.fingerprint = record.fingerprint;
    this.visitor = record.visitorID;
    this.os = `${record.browser.os} ${record.browser.osVersion ?? ''}`;
    this.browser = `${record.browser.browserName} ${record.browser.browserFullVersion ?? ''}`;
    this.visits = record.visits?.length ?? 0;
    this.city = record.city?.name ?? '';
    this.country = record.country?.name ?? '';
    this.subdivision = record.subdivision?.name ?? '';
    this.incognito = record.incognito ?? false;
    this.visitTime = record.time;
    this.timestamp = record.timestamp;
    this.isoDate = record.isoDate;
  }
}
