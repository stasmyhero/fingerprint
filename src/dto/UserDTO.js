import { BaseDTO } from './BaseDTO.js';

export class UserDTO extends BaseDTO {
  constructor(user) {
    super(user);
    this.email = user.email;
    this.password = user.password;
  }
}
