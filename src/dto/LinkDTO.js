import { BaseDTO } from './BaseDTO.js';

export class LinkDTO extends BaseDTO {
  constructor(link) {
    super(link);
    this.date = link.created_at;
    this.url = link.url;
    this.recipient = link.recipient ?? null;
    this.creator = link.creator ?? 'admin';
  }
}
