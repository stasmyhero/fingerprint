import { config } from 'dotenv';
config();
import pino from 'pino';
import expressPino from 'express-pino-logger';
import express from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import mongoose from 'mongoose';
import { authRouter } from './src/routers/authRouter.js';
import { recordRouter } from './src/routers/recordsRouter.js';
import { linkRouter } from './src/routers/linkRouter.js';
import { errorHandler } from './src/middleware/errorHandler.js';
import { ApiError } from './src/errors/ApiError.js';

const app = express();

/** middleware */
app.use(express.json());
app.use(cookieParser());
app.use(
  cors({
    credentials: true,
    origin: process.env.APP_CLIENT_URL
  })
);
app.use(expressPino(pino({ level: 'info' })));

/** routes */
app.use('/auth', authRouter);
app.use('/record', recordRouter);
app.use('/link', linkRouter);
/** error handling */
app.use((_res, _req, next) => {
  next(ApiError.NotFound());
});
app.use(errorHandler);

const start = async () => {
  console.clear();
  await mongoose.connect(process.env.DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  app.listen(process.env.APP_PORT || 3001, () => console.log(`Server started at ${process.env.APP_PORT || 3001}`));
};

start();
